new Vue({
  el: '#app',
  vuetify: new Vuetify(),

  data: {
    createDialogActive: false,
    editDialogActive: false,
    person: {},
    persons: [
      {
        id: 0,
        name: 'Mike',
        email: 'mike@gmail.com',
        avatar: 'http://www.shipwlgroup.com/wp-content/uploads/2016/08/1-4.jpg'
      },
      {
        id: 1,
        name: 'Sam',
        email: 'sam@gmail.com',
        avatar: ''
      }
    ],
    filter: '',
    filterBy: 'name'
  },

  computed: {
    disabled: function() {
      return !this.person.name || !this.person.email;
    }
  },

  methods: {
    showCreateDialog: function() {
      this.person = {};
      this.createDialogActive = true;
    },

    showEditDialog: function(index) {
      this.person = Object.assign({}, this.persons[index]);
      this.editDialogActive = true;
    },

    create: function(person) {
      person.id = this.persons[this.persons.length - 1].id + 1;
      this.persons.push(person);
      this.person = {};
      this.createDialogActive = false;
    },
    update: function(person) {
      this.persons[person.id] = Object.assign(this.persons[person.id], person);
      console.log(person);
      this.person = {};
      this.editDialogActive = false;
    },

    destroy: function(index) {
      Vue.delete(this.persons, index);
    },

    filteredPersonsBy: function(filterBy) {
      return this.persons.filter(person => {
        return person[filterBy].toLowerCase().includes(this.filter.toLowerCase())
      });
    }
  },
});